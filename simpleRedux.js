const createStore=(reducer, init)=>{
    const store={}
    store.state=init
    store.listeners=[]
    
     store.getState=()=>{
          return store.state}
    store.subscribe= (listener)=>store.listeners.push(listener) // automatically return keyword use.
    
    store.dispatch=action=>{
     store.state=reducer(store.state, action)
     store.listeners.forEach(listener=>listener())
    }
    
        return store
    }// a function is complete
    
    //uses of our redux store
    const reducer =(state,action)=>{
        switch (action.type) {
            case 'ADD':{
                return state + action.payload

            }
                
                break;
                case 'SUB':{
                    return state-action.payload
                
                }
                    
                    break;
        
            default:
                break;
        }
    
    }
    const store= createStore(reducer,0)
    
    store.subscribe(()=>{
        console.log(store.getState());
    })
    store.dispatch({type: "ADD", payload: 100})
    store.dispatch({type: "SUB", payload:50})
    store.dispatch({type: "SUB", payload:50})
    store.dispatch({type: "SUB", payload:50})
    store.dispatch({type: "ADD", payload: 100})
    
    
    
//     //the na is call return. but not use return keyword
//    var  na="Muhammad Nazmul Hossain"
// const name =()=>na


// var h=name();
// console.log(h);
    
    
    
    